# 04. HTML 텍스트 기본 태그 1

## [예제]
![가사 페이지 예제 결과](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/02-html/04/screenshots/song.png)   

* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/04/song-lecture.html" target="_blank">상어가족 예제 실습 코드</a>
* <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/blob/master/2020-spring/02-html/04/song-complete.html" target="_blank">상어가족 예제 완성본 코드</a>

<br>
  
***

<br>

## 제목, 부(...)제목 태그
```
단계별 구획의 제목을 나타내기 위한 태그입니다.
```

<br>

### `<h1>` ~ `<h6>`
* 번호별로 크기가 달라짐 (높을수록 하위 제목)   
* 줄바꿈 적용

#### 권장사항
1. h1는 페이지당 하나  
2. 순서를 건너뛰지 말 것 (예: `<h1>` 다음에는 `<h2>`)  
3. ~~시각적 효과~~를 위해 사용하지 말 것 (**디자인은 CSS로**)  
4. 정보의 구조를 나타내는 용도로 사용하 것

<br>

***

<br>

## 줄바꿈 태그
### `<br>` vs. `<p>`

#### `<br>` : break
* 닫는 태그 필요 없음, 위치 무관
* 여러번 사용 → 여러 줄
* 단순 시각적인 줄 바꿈 `(엔터)`

#### `<p>` : paragraph
* 정보 부여: 한 문단, 정보 덩어리임을 나타냄  
* **CSS가 적용될 단위**로 사용 
* 단순 줄 바꿈이 아닌, **문단 구분**을 위한 태그

<br>

***

<br>

## 강조 태그
### `<b>` vs. `<strong>`

#### `<b>` : bold
* 단순히 진하게

#### `<strong>` 
* 정보적 강조 (컴퓨터에게 **중요한 정보**임을 알림)
>   - `예` 스크린 리더가 강하게 발음 

<br>

***

<br>

## 기울임 태그
### `<i>` vs. `<em>`

#### `<i>` : italic
* 단순히 기울임

#### `<em>` : emphasize
* 정보적 강조 (컴퓨터에게 **중요한 정보**임을 알림)
>   + `예` 스크린 리더가 강하게 발음 

<br>

***

<br>

## 기타 태그

### `<small>` 
* 글씨를 작게

### `<cite>` : citation
* 저작물의 출처 표기 (**제목**을 표시할 것)

<br>

***

<br>

## 기억하세요!
> HTML 태그로 ~~디자인~~을 하지 말 것 (디자인은 CSS로)  
> HTML 태그들의 주 용도: `정보의 구조와 특성` 표현 

<br>

***

<br>

## 다음 강좌
* [05. HTML 기본 태그 2](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/05/README.md)
