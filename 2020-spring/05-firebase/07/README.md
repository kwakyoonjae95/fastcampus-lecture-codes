# 07. Firebase 상세페이지와 댓글 만들기 

## 상세페이지 만들기

<br>

> 선택된 사진 변수
```javascript
var selectedPhoto;
```

<br>

> `showPhotos` 함수에 추가
```javascript
photoNode.addEventListener('click', function () {
  selectedPhoto = photo;
  document.querySelector("main").className = 'detail';
})
```

<br>

> `detail` 섹션 추가
```html
<section id="detail" class="dep _detail">
</section>
```

<br>

> `css`에 `detail` 관련 추가
```css
main .dep { display: none; }
main.gallery .dep._intro,
main.gallery .dep._gallery,
main.upload .dep._upload,
main.myinfo .dep._myinfo,
main.detail .dep._detail {
  display: block;
}
```

<br>

> `aside`에 뒤로가기 버튼 추가
```html
<div id="backToGallery" class="dep _detail option-title" onclick="setMenu('gallery')">◀︎ 뒤로가기</div>
```

<br>

> `css`에 뒤로가기 버튼 스타일 추가
```css
#backToGallery {
  cursor: pointer;
}
#backToGallery:hover {
  opacity: 0.8;
}
```

<br>

> `detail` 섹션에 내용 요소들 넣기
```html
<section id="detail" class="dep _detail">
  <div class="photo"></div>
  <div class="author"></div>
  <div class="desc"></div>
</section>
```

<br>

> `photoNode`의 이벤트에 `detail` 채우기 추가
```javascript
photoNode.addEventListener('click', function () {
  // ...
  var detailSection = document.querySelector("#detail");
  detailSection.querySelector(".photo").style.backgroundImage 
  = "url('" + photo.url + "')";
  detailSection.querySelector(".author").innerHTML = photo.user_name;
  detailSection.querySelector(".desc").innerHTML = photo.description;
});
```

<br>

> 내용부 스타일 추가
```css
#detail {
  padding: 32px;
}
#detail .photo {
  width: 660px;
  height: 480px;
  background-size: cover;
  background-position: 50% 50%;
}
#detail .author {
  margin-top: 24px;
  font-size: 1.4rem;
  font-weight: bold;
}
#detail .desc {
  font-size: 1.1rem;
  margin-top: 8px;
}
```

<br>
<br>

***

<br>
<br>

## 댓글 달기 기능 만들기

<br>

> `detail` 섹션에 댓글 입력창 추가
```html
<section id="detail" class="dep _detail">
  <!-- ... -->
  <br>
  <input id="comment-input" type="text" placeholder="댓글을 작성해주세요.">
  <div id="comment-submit-button">올리기</div>
</section>
```

<br>

> `css`에 댓글 입력창 스타일 추가
```css
#comment-input {
  font-size: 1rem;
  width: 560px;
  height: 32px;
  padding: 0 8px;
  line-height: 32px;
}
#comment-submit-button {
  display: inline-block;
  height: 36px;
  line-height: 36px;
  padding: 0 16px;
  color: white;
  background-color: #ff4500;
  vertical-align: top;
  cursor: pointer;
}
#comment-submit-button:hover {
  opacity: 0.8;
}
```

<br>

> `uploadComment` 함수 추가 및 작성
```javascript
function uploadComment () {
  var comment = {
    idx: Date.now(),
    photo_idx: selectedPhoto.idx,
    user_id: my_info.id,
    user_name: my_info.user_name,
    comment: document.querySelector("#comment-input").value
  }

  db.collection("comments").doc(String(comment.idx)).set(comment)
  .then(function () {
    console.log("Success!");
  })
  .catch(function (error) {
    console.error("Error!", error);
  });
}
```

<br>

> 댓글을 올린 후 댓글창 비워주기 
```javascript
function uploadComment () {
  // ...
  document.querySelector("#comment-input").value = '';
}
```

<br>

> `loadComments` 함수 만들기
* 상세화면 여는 코드와 `uploadComment` 결과부에 각각 연결
```javascript
function loadComments () {
}
```

<br>

> 해당 사진의 댓글들 정보 받아오기
```javascript
function loadComments () {
  db.collection("comments").where("photo_idx", "==", selectedPhoto.idx).get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      console.log(doc.data());
    })
  })
}
```

<br>

> 받아온 댓글들을 역순으로 정렬
```javascript
function loadComments () {
  db.collection("comments").where("photo_idx", "==", selectedPhoto.idx).get().then(function (querySnapshot) {
    var comments = [];
    querySnapshot.forEach(function (doc) {
      comments.push(doc.data());
    })
    comments.sort(function (a, b) {
      return a.idx > b.idx ? -1 : 1;
    })
    console.log(comments);
  })
}
```

<br>

> `detail` 섹션에 댓글 달리는 공간 만들기
```html
<section id="detail" class="dep _detail">
  <!-- ... -->
  <div id="comments"></div>
</section>
```

<br>

> `loadComments` 함수에 기존 댓글 비우고 받아온 댓글들 넣는 코드 추가
```javascript
document.querySelector("#comments").innerHTML = '';

function loadComments () {
  db.collection("comments").where("photo_idx", "==", selectedPhoto.idx).get().then(function (querySnapshot) {
    
    // ...

    comments.forEach(function (comment) {
      var commentArticle = document.createElement("article");
      var commentInfoDiv = document.createElement("div");

      commentInfoDiv.className = "comment-info"
      commentInfoDiv.innerHTML = comment.user_name;

      var dateSpan = document.createElement("span");
      dateSpan.innerHTML = new Date(comment.idx);

      commentInfoDiv.appendChild(dateSpan);

      var commentContentDiv = document.createElement("div");
      commentContentDiv.innerHTML = comment.comment;

      commentArticle.append(commentInfoDiv);
      commentArticle.append(commentContentDiv);

      document.querySelector("#comments").append(commentArticle);
    });
  })
}
```

<br>

> `css`에 댓글 스타일 추가
```css
#comments {
  margin-top: 18px;
  width: 660px;
}
#comments > article {
  padding: 16px 12px;
}
#comments > article:not(:last-child) {
  border-bottom: 1px solid #ddd;
}
#comments > article .comment-info {
  margin-bottom: 6px;
  font-weight: bold;
}
#comments > article .comment-info span {
  margin-left: 8px;
  font-weight: normal;
  font-size: 0.9rem;
  color: gray;
}
```