# 05. 좋아요와 팔로우 기능 넣기

## 데이터 세팅
### 1. 지난 강의의 `uploadPhotoInfo` 함수에서 `id`를 `idx`로 수정
```javascript
function uploadPhotoInfo (url) {
  var photoInfo = {
    idx: Date.now(),
    // ...
  db.collection("photos").doc(String(photoInfo.idx)).set(photoInfo)
  // ...
```
### 2. `photos` 데이터와 스토리지 내 사진들 삭제
### 3. 기존 `photos`의 데이터대로 사진 업로드 뒤 콘솔에서 `user_idx`와 `user_name` 변경 

<br>

## 좋아요 기능 적용

### 1. `toggleLikeOnDB` 함수 만들기
```javascript
function toggleLikeOnDB (photo) {
  // ...
}
```
> toggleLike 함수 변경
```javascript
function toggleLike(idx) {
  // ...
        photos[i].likes++;
        toggleLikeOnDB(photos[i]);
        // ...
        photos[i].likes--;
        toggleLikeOnDB(photos[i]);
        // ...
  // showPhotos();
}
```

<br>


### 2. `my_info`의 `like` 항목 업데이트
```javascript
function toggleLikeOnDB (photo) {
  db.collection("my_info").doc(my_info.docId).update({
    like: my_info.like
  }).then(function () {
  });
}
```

### 3. 해당 사진의 `likes` 항목 업데이트
```javascript
function toggleLikeOnDB (photo) {
  db.collection("my_info").doc(my_info.docId).update({
    like: my_info.like
  }).then(function () {
    db.collection("photos").doc(String(photo.idx)).update({
      likes: photo.likes
    }).then(function () {
      loadPhotos();
    });
  });
}
```

<br>

## 팔로우 기능 추가


### 1. 팔로우 나타내는 텍스트 추가
> `showPhotos` 함수에 코드 추가
```javascript
// ...
if (my_info.follow.indexOf(photo.user_id) > -1) {
  var followSpan = document.createElement("span");
  followSpan.innerHTML = "FOLLOW"
  photoNode.querySelector(".author").append(followSpan);
}
// ...
```
> `css` 변경
```css

article .info .author {
  /* ... */
  cursor: pointer;
}

article .info .author:hover {
  opacity: 0.8;
}

article .info .author span {
  margin-left: 6px;
  font-size: 0.9rem;
  color: dodgerblue;
}
```

<br>

### 2. 팔로우 토글 기능 추가
> `toggleFollow` 함수 생성
```javascript
function toggleFollow (user_id) {
  // ...
}
```
> `showPhotos`에서 이벤트 연결
```javascript
// ...
photoNode.querySelector(".author").addEventListener(
  "click", function () { toggleFollow(photo.user_id) }
)
// ...
```
> `toggleFollow` 내용 작성
```javascript
function toggleFollow (user_id) {
  if (my_info.follow.indexOf(user_id) === -1) {
    my_info.follow.push(user_id);
  } else {
    my_info.follow = my_info.follow.filter(
      function (it) { return it !== user_id; }
    );
  }
  db.collection("my_info").doc(my_info.docId).update({
    follow: my_info.follow
  }).then(function () {
    loadPhotos();
  });
}
```

<br>

***

<br>

## 다음 강좌
* [06. Firebase 서버에서 정렬과 필터 적용하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/06/README.md)