# 06. CSS grid 레이아웃

페이지를 구획으로 나누는 다양한 방법  
> * Gitlab (현 페이지)  
> * MDN - <a href="https://developer.mozilla.org/ko/docs/Web/CSS/CSS_Grid_Layout" target="_blank">[grid layout]</a>

<br>

```html
  <div class="wrapper">
    <div>1</div>
    <div>2</div>
    <div>3</div>
    <div>4</div>
    <div>5</div>
    <div>6</div>
    <div>7</div>
    <div>8</div>
    <div>9</div>
  </div>
```
```css
/* 기본세팅 */
body { margin: 0; padding: 0; }
.wrapper { border: 2px solid black; }
.wrapper > div { 
  background-color: yellow; 
}
.wrapper > div:nth-child(even) { 
  background-color: yellowgreen;
}
```
<br>


## 부모 요소를 grid 디스플레이로
```css {
.wrapper {
  display: grid;
}
```
<br>
<br>

## 열의 갯수와 크기 지정 
`grid-template-columns`

<br>

>절대값으로
```css
.wrapper {
  grid-template-columns: 100px 200px 300px;
}
```  
<br>

> 비율 분할(fractions)
```css
.wrapper {
  grid-template-columns: 1fr 2fr 3fr 4fr;
}
```
<br>

> 혼합 사용 
```css
.wrapper {
  grid-template-columns: 200px 1fr 3fr;
}
```
<br>

> 반복 지정 
```css
.wrapper {
  grid-template-columns: repeat(3, 1fr);
}
```

<br>
<br>

## 행의 갯수와 크기 지정 
`grid-template-rows`

> 절대값으로
```css
.wrapper {
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 100px 200px 400px;
}
```
<br>

> 상대값은 높이가 지정되어 있을 때 
```css
.wrapper {
  grid-template-columns: repeat(3, 1fr);
  height: 800px;
  grid-template-rows: 1fr 2fr 3fr;
}
```
<br>

> 내부 컨텐츠의 높이에 따라
```html
  <div class="wrapper">
    <div><div>1</div></div>
    <div><div>2</div></div>
    <div><div>3</div></div>
    <div><div>4</div></div>
    <div><div>5</div></div>
    <div><div>6</div></div>
    <div><div>7</div></div>
    <div><div>8</div></div>
    <div><div>9</div></div>
  </div>
```
```css
/* 전용 세팅 */
.wrapper {
  grid-template-columns: repeat(3, 1fr);
}
.wrapper > div > div { background-color: orange; }
.wrapper > div:nth-child(even) > div { background-color: tomato; }
.wrapper > div:nth-child(3n + 1) > div {
  height: 100px;
}
.wrapper > div:nth-child(3n + 2) > div {
  height: 200px;
}
.wrapper > div:nth-child(3n) > div {
  height: 300px;
}
```

```css
.wrapper {
  grid-template-rows: 100px auto minmax(100px, 200px);
}
```

<br>
<br>

![grid 예제 결과](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/03-css/06/screenshots/grided.png)   

## 선 번호로 영역 지정 
`grid-column`, `grid-row`

```html
  <div class="wrapper">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
```
```css
/* 기본 세팅 */
.wrapper {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(4, 150px);
}
.wrapper > div { 
  background-color: yellow; 
}
.wrapper > div:nth-child(even) { 
  background-color: yellowgreen;
}
```
<br>

> 아래부터
```css
.wrapper > div:nth-child(1) {
  background-color: red;
  grid-column: 1/4;
  grid-row: 1/2;
}
.wrapper > div:nth-child(2) {
  background-color: green;
  grid-column: 1/2;
  grid-row: 2/4;
}
.wrapper > div:nth-child(3) {
  background-color: purple;
  grid-column: 2/4;
  grid-row: 2/4;
}
.wrapper > div:nth-child(4) {
  background-color: blue;
  grid-column: 1/3;
  grid-row: 4/5;
}
.wrapper > div:nth-child(5) {
  background-color: orange;
  grid-column: 3/4;
  grid-row: 4/5;
}
```

<br>
<br>


## 이름으로 영역 지정 
`grid-template-rows`

```css
.wrapper {
  grid-template-areas: 
    "red red red"
    "green blue blue"
    "green blue blue"
    "purple purple orange";
}
.wrapper > div:nth-child(1) {
  background-color: red;
  grid-area: red;
}
.wrapper > div:nth-child(2) {
  background-color: green;
  grid-area: green;
}
.wrapper > div:nth-child(3) {
  background-color: blue;
  grid-area: blue;
}
.wrapper > div:nth-child(4) {
  background-color: purple;
  grid-area: purple;
}
.wrapper > div:nth-child(5) {
  background-color: orange;
  grid-area: orange;
}
```

<br>
<br>

## grid간 공간 띄우기

```css
.wrapper {
  gap: 10px;
}
```

<br>
<br>

## 더 자세히 알아보기
> <a href="https://developer.mozilla.org/ko/docs/Web/CSS/CSS_Grid_Layout" target="_blank">MDN - Grid 레이아웃</a>  
> <a href="https://developer.mozilla.org/ko/docs/Web/CSS/CSS_Flexible_Box_Layout" target="_blank">MDN - Flexible Box 레이아웃</a>

<br>
<br>

## 다음 강좌
* [07. CSS Material Design](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/07/README.md)